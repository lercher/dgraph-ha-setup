# dgraph high availability setup

This repo demonstrates a simple
HA setup consisting of two physical
linux nodes `linux-home` called NODE1
and `linux-pm81` called NODE2
running three zero and three alpha nodes
in total. One of each kind on NODE1 and
two of which on NODE2. The repo basicly 
gathers the six command lines used to
start the six processes and explains,
why they are needed.

Everything is in the documentation, though.
However it's not explained in a way that
I'm comfortable with, i.e. I'm too dumb for
that. Roughly speeking, if you leave out
some of the command line options, it won't
work and diagnostics are not that helpful.
I.e. there are a lot of funny log messages saying
that something went wrong, but not what to do to resolve
the error state.

## Ressources

* [https://dgraph.io/](https://dgraph.io/)
* [https://docs.dgraph.io/deploy/#multi-host-setup](https://docs.dgraph.io/deploy/#multi-host-setup)
* [https://docs.dgraph.io/deploy/#ports-usage](https://docs.dgraph.io/deploy/#ports-usage)
* [https://docs.dgraph.io/deploy/#more-about-dgraph-zero](https://docs.dgraph.io/deploy/#more-about-dgraph-zero)
 
## Zero

On NODE2 in a second subdirectory with two zero processes

````sh
exec dgraph zero --peer linux-home:5080 --my linux-pm81:5081 --idx 1 --replicas 3 -o 1
````

cmd line part | meaning | comment
--------------|---------|------------
`exec dgraph zero` | Replace the shell with a dgraph zero process | Standard exec semantics
`--peer linux-home:5080` | Join the cluster by contacting the already running zero process on NODE1 at the standard port 5080 | Don't specify for the first zero node
`--my linux-pm81:5081` | That's me, i.e. other zero nodes initiate connections to this address* | If left out, other nodes want to communicate with localhost, which is plainly wrong
`--idx 1` | The number `1` must be unique in the whole cluster | If missing the second zero node errors out with `duplicate id`
`--replicas 3` | We want to use three alphas for each predicate. That's basicly, where HA happens. | If missing, predicates are not replicated
`-o 1` | Port offset of 1 | Shift standard communication port number by one, i.e. 5081, 6081 instead of 5080 and 6080. Not needed for a physical node. Used here to avoid a clash of two different zeros running on the same hardware.

Zero creates the subdirectory `zw` to store its data,
unless you direct it to a different store.


## Alpha

On NODE1 in subdirectory with one alpha process and one zero process

````sh
exec dgraph alpha --zero localhost:5080 --my linux-home:7080
````

cmd line part | meaning | comment
--------------|---------|------------
`exec dgraph alpha` | Replace the shell with a dgraph alpha process | Standard exec semantics
`--zero localhost:5080` | There is an already running zero process on NODE1 at the standard port 5080. | Most probably the default, but needed for the 2nd alpha on NODE2
`--my linux-home:7080` | That's me, i.e. other alpha/zero nodes initiate connections to this address* | If left out, other nodes want to communicate with localhost, which is plainly wrong

Alpha creates the subdirectories `p` and `w` to store
predicates and the write ahead log,
unless you direct it to different stores.

## *Myself

````sh
hostname -A | cut -f -d " "
````

## Removing a failed node

See [https://docs.dgraph.io/deploy/#more-about-dgraph-zero](https://docs.dgraph.io/deploy/#more-about-dgraph-zero)
for details. This is the relevant quote:

Zero also exposes HTTP on 6080 ... 
You can query (GET request) it to see useful information, like the following:
...

`/removeNode?id=3&group=2` If a replica goes down and can’t be recovered, 
you can remove it and add a new node to the quorum. This endpoint can
be used to remove a dead Zero or Dgraph Alpha node. To remove dead 
Zero nodes, pass `group=0` and the id of the Zero node.

